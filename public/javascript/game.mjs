let ready = false;

const username = sessionStorage.getItem('username');

if (!username) {
    window.location.replace('/login');
}

const socket = io('', { query: { username } });

socket.emit('new-user', { username: username });

socket.on('join', (data) => {
    console.log('new user: ', data.user);
    newUser(data.user);

    // addMessage(data, 'server');
});

socket.on('exist user', (data) => {
    sessionStorage.removeItem('username');
    window.location.replace('/login');
});



const newUser = (username) => {
    const div = document.createElement('div');
    div.classList.add('playercard');

    div.innerHTML = `<div class="ready">
        <span class="readydot"></span>
      </div>
         <div class="username">${username} #you </div>
        <progress class="progressbar" value=10>Progress bar</progress>`;

    document.querySelector('.players').appendChild(div);
};

const disconnectButton = document.getElementById('leave-btn');
const readyButton = document.getElementById('ready-btn');

const onClickReady = () => {
    document.querySelector(`.readydot`).style.background = 'green';
};

const onClickDisconnectButton = () => {
    const leaveRoom = confirm('Are you sure you want to leave ?');
    if (leaveRoom) {
        sessionStorage.removeItem('username');
        window.location.replace('/login');
    } else {
    }
};

disconnectButton.addEventListener('click', onClickDisconnectButton);
readyButton.addEventListener('click', onClickReady);
