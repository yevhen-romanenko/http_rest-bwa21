import * as config from './config';
const userSocketIdMap = new Map();

export default (io) => {
    io.on('connection', (socket) => {
        const username = socket.handshake.query.username;
        console.log(`${username} just connected with socket.id: `, socket.id);

        addClientToMap(username, socket.id);
        console.log(userSocketIdMap);

        socket.on('new-user', (data) => {
            console.log(`new user connection: ${data.username}`);
            socket.emit('join', { user: data.username, message: `Welcome to this Typerace game ${data.username}` });
        });

        socket.on('disconnect', () => {
            // console.log(`${username} disconnected with socket.id: `, socket.id);
            removeClientFromMap(username, socket.id);
        });

        function addClientToMap(userName, socketId) {
            if (!userSocketIdMap.has(userName)) {
                userSocketIdMap.set(userName, new Set([socketId]));
            } else {
                console.log('User with this name already exist!');
                socket.emit('exist user', { user: userName });
            }
        }

        function removeClientFromMap(userName, socketId) {
            if (userSocketIdMap.has(userName)) {
                let userSocketIdSet = userSocketIdMap.get(userName);
                userSocketIdSet.delete(socketId);

                if (userSocketIdSet.size == 0) {
                    userSocketIdMap.delete(userName);
                }
            }
        }
    });
};
